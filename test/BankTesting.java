/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import bank.Bank;
import bank.Person;
import bank.SavingAccount;
import bank.SpendingAccount;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Xtra Sonic
 */
public class BankTesting {
    
    public BankTesting() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
	public void test1() throws IOException, FileNotFoundException, ClassNotFoundException 
	{
		Bank b=new Bank("test");
		Person p=new Person("Test","Person");
                b.addPerson(p);
		SpendingAccount sa=new SpendingAccount(0);
		b.addAccountToPerson(p, sa);
		sa.deposit(300);
		assertTrue(b.readAccountData(p)[0].getBalance()==300);
	}
	
	@Test
	public void test2() throws IOException, FileNotFoundException, ClassNotFoundException 
	{
		
		Bank b=new Bank("test");
		Person p=new Person("Test","Person");
                b.addPerson(p);
		SavingAccount sa=new SavingAccount(0,SavingAccount.ONE_MONTH);
		b.addAccountToPerson(p, sa);
		sa.deposit(300);
                boolean failed=false;
                try{
                    b.readAccountData(p)[0].withdraw(100);
                }
                catch(IllegalArgumentException | IllegalStateException iae)
                {
                    failed=true;
                }
		assertTrue(failed);
	}
        
        @Test
	public void test3() throws IOException, FileNotFoundException, ClassNotFoundException 
	{
		
		Bank b=new Bank("test");
		Person p=new Person("Test","Person");
                b.addPerson(p);
                p=new Person("Test2","Person");
                b.addPerson(p);
                p=new Person("Test3","Person");
                b.addPerson(p);
                
                assertTrue(b.getPersons().length==3);
	}
        
        @Test
        public void test4() throws IOException, FileNotFoundException, ClassNotFoundException 
	{
		Bank b=new Bank("test");
		Person p=new Person("Test","Person");
                b.addPerson(p);
		SpendingAccount sa=new SpendingAccount(0);
		b.addAccountToPerson(p, sa);
                sa=new SpendingAccount(1);
		b.addAccountToPerson(p, sa);
                sa=new SpendingAccount(2);
		b.addAccountToPerson(p, sa);
                
		SavingAccount savea=new SavingAccount(SavingAccount.ONE_MONTH,3);
		b.addAccountToPerson(p, savea);
                savea=new SavingAccount(SavingAccount.ONE_MONTH,4);
		b.addAccountToPerson(p, savea);
                savea=new SavingAccount(SavingAccount.ONE_MONTH,5);
		b.addAccountToPerson(p, savea);
		
                
                assertTrue(b.readAccountData(p).length==6);
	}
        
        @Test
        public void test5() throws IOException, FileNotFoundException, ClassNotFoundException 
	{
            
        
        }
        
	
}
