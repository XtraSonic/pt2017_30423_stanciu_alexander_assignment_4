/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Xtra Sonic
 */
public class SpendingAccount extends Account{

    public SpendingAccount(int id)
    {
        super(id);
    }
    
    @Override
    public void deposit(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negative amount was introduced");
        }
        balance+=amount;
        setChanged();
        notifyObservers();
    }

    @Override
    public void withdraw(int amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Negative amount was introduced");
        }
        if (balance - amount < 0) {
            throw new IllegalArgumentException("Not enough money in balance");
        }
        balance -=amount;
        setChanged();
        notifyObservers();
    }

}
