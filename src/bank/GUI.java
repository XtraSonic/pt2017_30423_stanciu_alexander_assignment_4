/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Xtra Sonic
 */
public class GUI {

    public static void main(String[] args) {
        GUI.start();
    }

    private static GUI GUISingleton;
    JFrame bankFrame;
    JPanel bankPanel;
    JScrollPane personScrollPane;
    JScrollPane accountScrollPane;
    JTable personTable;
    JTable accountTable;
    private static final int WITDH = 1550;
    private static final int HEIGHT = 400;
    Dimension dimension = new Dimension(WITDH, HEIGHT);
    JButton addPersonBtn;
    JButton removePersonBtn;
    JButton removeAccountBtn;
    JButton addAccountBtn;
    JButton withdrawBtn;
    JButton depositBtn;
    JButton saveBtn;

    JTextField firstNameTF;
    JTextField lastNameTF;
    JTextField amountTF;
    JTextField selectedPersonTF;
    JTextField selectedAccountTF;

    JComboBox accountTypeCB;
    JComboBox planCB;

    JLabel firstNameL;
    JLabel lastNameL;
    JLabel accountTypeL;
    JLabel planL;
    JLabel amountL;

    Bank bank;
    Person[] personArray;
    Account[] accountArray;
    int selectedPerson = -1;
    int selectedAccount = -1;

    private GUI() {
        try {
            bank = new Bank("Bank.txt");
        } catch (IOException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(bankFrame, "Failed to get Bank from file");
            return;
        }

        bankPanel = new JPanel();
        bankFrame = new JFrame("Bank");
        bankPanel.setPreferredSize(dimension);

        Dimension buttonDim = new Dimension(WITDH / 4, 25);
        Dimension labelDim = new Dimension(100, 25);

        addPersonBtn = new JButton("Add new Person");
        removePersonBtn = new JButton("Remove selected Person");
        addAccountBtn = new JButton("Add new Accont to the selected person");
        removeAccountBtn = new JButton("Remove the selected account");
        withdrawBtn = new JButton("Withdraw from the selected account");
        depositBtn = new JButton("Deposit to the selected account");
        saveBtn = new JButton("Save");
        addPersonBtn.setPreferredSize(buttonDim);
        removePersonBtn.setPreferredSize(buttonDim);

        firstNameTF = new JTextField(40);
        lastNameTF = new JTextField(40);
        amountTF = new JTextField(20);
        selectedPersonTF = new JTextField(40);
        selectedPersonTF.setEnabled(false);
        selectedPersonTF.setBackground(Color.BLACK);
        selectedPersonTF.setText("The selected person is: ");
        selectedAccountTF = new JTextField(40);
        selectedAccountTF.setEnabled(false);
        selectedAccountTF.setBackground(Color.BLACK);
        selectedAccountTF.setText("The selected account has the id: ");

        String[] types = {"Spending Account", "Saving Account"};
        String[] plans = {"One Month", "Three Months", "One Year"};
        accountTypeCB = new JComboBox(types);
        planCB = new JComboBox(plans);

        firstNameL = new JLabel("First name");
        lastNameL = new JLabel("Last name");
        accountTypeL = new JLabel("Account Type");
        planL = new JLabel("Plan");
        amountL = new JLabel("Amount");

        /*amountL.setPreferredSize(labelDim);
        firstNameL.setPreferredSize(labelDim);
        lastNameL.setPreferredSize(labelDim);
         */
        bankPanel.add(addPersonBtn);
        bankPanel.add(firstNameL);
        bankPanel.add(firstNameTF);
        bankPanel.add(lastNameL);
        bankPanel.add(lastNameTF);

        bankPanel.add(selectedPersonTF);
        bankPanel.add(removePersonBtn);
        bankPanel.add(addAccountBtn);
        bankPanel.add(accountTypeL);
        bankPanel.add(accountTypeCB);
        bankPanel.add(planL);
        bankPanel.add(planCB);
        bankPanel.add(selectedAccountTF);
        bankPanel.add(removeAccountBtn);

        bankPanel.add(amountL);
        bankPanel.add(amountTF);
        bankPanel.add(withdrawBtn);
        bankPanel.add(depositBtn);
        bankPanel.add(saveBtn);

        cretePersonTable();
        bankPanel.add(personScrollPane);
        creteAccountTable(null);
        bankPanel.add(accountScrollPane);

        bankFrame.getContentPane().add(bankPanel);
        bankFrame.pack();
        bankFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        bankFrame.setVisible(true);

        /*Person p=new Person("Bob", "Bobson");
        bank.addPerson(p);
        bank.addAccountToPerson(p,new SavingAccount(0,1));
        bank.addAccountToPerson(p,new SpendingAccount(2));
        bank.addAccountToPerson(p,new SavingAccount(1,3));
        
        updatePersons();
        System.out.println(bank.toString());
        updateAccounts(p);
        selectedPerson=0;
        selectedAccount=1;
        selectedPersonTF.setText("The selected person is: "+personArray[selectedPerson].toString());
        selectedAccountTF.setText("The selected account has the id: "+accountArray[selectedAccount].getId());
        Mendokusai
         */
        addPersonBtn.addActionListener((ActionEvent e) -> {
            if (firstNameTF.getText().equals("") || lastNameTF.getText().equals("")) {
                JOptionPane.showMessageDialog(bankFrame, "Name field(s) empty");
                return;
            }
            Person person = new Person(firstNameTF.getText(), lastNameTF.getText());
            bank.addPerson(person);
            updatePersons();

        });
        removePersonBtn.addActionListener((ActionEvent e) -> {
            if (selectedPerson == -1|| personArray==null || selectedPerson >= personArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid person selected");
                return;
            }
            bank.removePerson(personArray[selectedPerson]);
            updatePersons();
        });
        removeAccountBtn.addActionListener((ActionEvent e)->{
            if (selectedPerson == -1|| personArray==null || selectedPerson >= personArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid person selected");
                return;
            }
            if (selectedAccount == -1 || accountArray==null || selectedAccount >= accountArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid account selected");
                return;
            }
            bank.removeAccountFromPerson(personArray[selectedPerson],accountArray[selectedAccount]);
            updateAccounts(personArray[selectedPerson]);
        });
        withdrawBtn.addActionListener((ActionEvent e)->{
            if (selectedPerson == -1|| personArray==null || selectedPerson >= personArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid person selected");
                return;
            }
            if (selectedAccount == -1 || accountArray==null || selectedAccount >= accountArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid account selected");
                return;
            }
            int amount;
            try{
                amount=Integer.parseInt(amountTF.getText());
                accountArray[selectedAccount].withdraw(amount);
            }
            catch(NumberFormatException nfe)
            {   
                JOptionPane.showMessageDialog(bankFrame, "InvalidAmount");
            }
            catch(IllegalArgumentException | IllegalStateException iae)
            {   
                JOptionPane.showMessageDialog(bankFrame, iae.getMessage());
            }
            int temp=selectedAccount;
            updateAccounts(personArray[selectedPerson]);
            selectedAccount=temp;
        });
        depositBtn.addActionListener((ActionEvent e)->{
            if (selectedPerson == -1|| personArray==null || selectedPerson >= personArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid person selected");
                return;
            }
            if (selectedAccount == -1 || accountArray==null || selectedAccount >= accountArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid account selected");
                return;
            }
            int amount;
            try{
                amount=Integer.parseInt(amountTF.getText());
                accountArray[selectedAccount].deposit(amount);
            }
            catch(NumberFormatException nfe)
            {   
                JOptionPane.showMessageDialog(bankFrame, "InvalidAmount");
            }
            catch(IllegalArgumentException | IllegalStateException iae)
            {   
                JOptionPane.showMessageDialog(bankFrame, iae.getMessage());
            }
            int temp=selectedAccount;
            updateAccounts(personArray[selectedPerson]);
            selectedAccount=temp;
        });
        addAccountBtn.addActionListener((ActionEvent e) -> {
            if (selectedPerson == -1|| personArray==null || selectedPerson >= personArray.length) {
                JOptionPane.showMessageDialog(bankFrame, "No valid person selected");
                return;
            }
            if (accountTypeCB.getSelectedIndex() == 0) {
                if (accountArray == null) {
                    Account a = new SpendingAccount(0);
                    bank.addAccountToPerson(personArray[selectedPerson], a);

                } else {
                    boolean ok;
                    for (int i = 0; i <= accountArray.length; i++) {
                        ok=true;
                        for (int j = 0; j < accountArray.length; j++) {
                            if (i == accountArray[j].getId()) {
                                ok=false;
                            }
                        }
                        if(ok)
                        {
                            Account a = new SpendingAccount(i);
                            bank.addAccountToPerson(personArray[selectedPerson], a);
                            break;
                        }
                    }

                }
            }
            else
            {
                if (accountArray == null) {
                    Account a = new SavingAccount(planCB.getSelectedIndex(), 0);
                    bank.addAccountToPerson(personArray[selectedPerson], a);

                } else {
                    boolean ok;
                    for (int i = 0; i <= accountArray.length; i++) {
                        ok=true;
                        for (int j = 0; j < accountArray.length; j++) {
                            if (i == accountArray[j].getId()) {
                                ok=false;
                            }
                        }
                        if(ok)
                        {
                            Account a = new SavingAccount(planCB.getSelectedIndex(),i);
                            //System.out.print(planCB.getSelectedIndex());
                            bank.addAccountToPerson(personArray[selectedPerson], a);
                            break;
                        }
                    }

                }
            }
            updateAccounts(personArray[selectedPerson]);
        });
        saveBtn.addActionListener((ActionEvent ae)->{
            try {
                bank.writeBank();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(bankFrame, "Could not save bank");
            }
        });
        
    }

    public static void start() {
        if (GUISingleton == null) {
            GUISingleton = new GUI();
        } else {
            throw new ExceptionInInitializerError();
        }
    }

    private void cretePersonTable() {

        selectedPerson = -1;
        selectedPersonTF.setText("The selected person is: ");
        personArray = bank.getPersons();
        Object[][] data = new Object[personArray.length][2];
        int index = 0;
        for (Person p : personArray) {
            data[index][0] = p.getFirstName();
            data[index][1] = p.getLastName();
            index++;
        }
        String[] colNames = {"First Name", "Last Name"};
        personTable = new JTable(data, colNames);
        personTable.setEnabled(false);
        personScrollPane = new JScrollPane(personTable);
        personScrollPane.setPreferredSize(new Dimension(WITDH / 2, 23 + 16 * (personArray.length)));
        personTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                selectedPerson = personTable.rowAtPoint(me.getPoint());
                selectedPersonTF.setText("The selected person is: " + personArray[selectedPerson].toString());
                if (selectedPerson >= 0) {
                    updateAccounts(personArray[selectedPerson]);
                }
            }

        });
    }

    private void updatePersons() {
        bankPanel.remove(personScrollPane);
        bankFrame.getContentPane().remove(bankPanel);
        cretePersonTable();
        bankPanel.add(personScrollPane);
        bankFrame.getContentPane().add(bankPanel);
        bankFrame.pack();
        bankFrame.repaint();
        updateAccounts(null);
    }

    private void creteAccountTable(Person p) {

        String[] colNames = {"ID", "Type", "Balance", "Plan", "Next Withdraw Time"};
        selectedAccountTF.setText("The selected account has the id: ");
        selectedAccount = -1;
        if (p == null) {
            accountTable = new JTable(new DefaultTableModel(colNames, 0));
            accountScrollPane = new JScrollPane(accountTable);
            accountScrollPane.setPreferredSize(new Dimension(WITDH / 2 - 10, 23));
            return;
        }
        accountArray = bank.readAccountData(p);
        Object[][] data = new Object[accountArray.length][5];
        int index = 0;
        for (Object o : accountArray) {
            if (o instanceof SavingAccount) {
                SavingAccount sa = (SavingAccount) o;
                data[index][0] = sa.getId();
                data[index][1] = "Saving Account";
                data[index][2] = sa.getBalance();
                data[index][3] = sa.getPlan();
                data[index][4] = sa.getEndTime();

            } else {

                SpendingAccount sa = (SpendingAccount) o;
                data[index][0] = sa.getId();
                data[index][1] = "Spending Account";
                data[index][2] = sa.getBalance();
                data[index][3] = "";
                data[index][4] = "";
            }

            index++;

        }
        accountTable = new JTable(data, colNames);
        accountTable.setEnabled(false);
        accountScrollPane = new JScrollPane(accountTable);
        accountScrollPane.setPreferredSize(new Dimension(WITDH / 2 - 10, 23 + 16 * (accountArray.length)));
        accountTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                selectedAccount = accountTable.rowAtPoint(me.getPoint());
                selectedAccountTF.setText("The selected account has the id: " + accountArray[selectedAccount].getId());
            }

        });
    }

    private void updateAccounts(Person p) {
        bankPanel.remove(accountScrollPane);
        bankFrame.getContentPane().remove(bankPanel);
        creteAccountTable(p);
        bankPanel.add(accountScrollPane);
        bankFrame.getContentPane().add(bankPanel);
        bankFrame.pack();
        bankFrame.repaint();
    }

}
