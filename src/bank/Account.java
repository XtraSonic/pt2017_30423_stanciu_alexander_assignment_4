/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package bank;

import java.io.Serializable;
import java.util.Observable;

/**
 *
 * @author Xtra Sonic
 */
public abstract class Account extends Observable implements Serializable {
    
    protected double balance;
    protected int id;
    
    public Account(int id)
    {
        super();
        this.id=id;
        balance = 0;
    }
    
    abstract public void deposit(double amount);
    abstract public void withdraw(int amount);

    public double getBalance() {
        return balance;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return this.getClass().getName()+"id="+id+" new balance:"+ balance +"$";
    }
    
    
    
    
}
