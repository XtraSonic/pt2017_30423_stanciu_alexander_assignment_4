/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author Xtra Sonic
 */
public class SavingAccount extends Account{

    public static final int ONE_MONTH = 0;
    public static final int THREE_MONTHS = 1;
    public static final int ONE_YEAR = 2;
    public static final double[] INTEREST_RATES = {0.15, 0.6, 3};

    private static final Duration[] times = {
        Duration.ZERO.plus(1, ChronoUnit.MINUTES),
        Duration.ZERO.plus(3, ChronoUnit.MINUTES),
        Duration.ZERO.plus(12, ChronoUnit.MINUTES)};

    private boolean currentlySaving;
    private int plan;
    private LocalDateTime start;

    public SavingAccount(int plan,int id) {
        super(id);
        if (plan > 2 || plan < 0) {
            throw new IllegalArgumentException("Invalid plan");
        }
        this.plan=plan;
        currentlySaving = false;
    }

    private void openDeposit() {
        currentlySaving = false;
        balance += balance * INTEREST_RATES[plan]/100;
        setChanged();
        notifyObservers();
    }

    public boolean isDepositSaving() {
        if (currentlySaving) {
            if (start != null && start.plus(times[plan]).isAfter(LocalDateTime.now())) {
                return true;
            }
            openDeposit();
        }
        return false;
    }

    @Override
    public void deposit(double amount) {
        if (currentlySaving) {
            if (start != null && start.plus(times[plan]).isAfter(LocalDateTime.now())) {
                throw new IllegalStateException("Savings Account is already in use until " + start.plus(times[plan]));
            }
            openDeposit();
        }
        if (amount < 0) {
            throw new IllegalArgumentException("Negative amount was introduced");
        }
        if (amount == 0 && balance == 0) {
            throw new IllegalArgumentException("Can`t start a deposit with 0 balance");
        }
        balance += amount;
        start = LocalDateTime.now();
        currentlySaving = true;
        setChanged();
        notifyObservers();

    }

    @Override
    public void withdraw(int amount) {

        if (currentlySaving) {
            if (start != null && start.plus(times[plan]).isAfter(LocalDateTime.now())) {
                throw new IllegalStateException("Savings Account is in use until " + start.plus(times[plan]));
            }
            openDeposit();
        }
        if (amount < 0) {
            throw new IllegalArgumentException("Negative amount was introduced");
        }
        if (balance - amount < 0) {
            throw new IllegalArgumentException("Not enough money in balance");
        }
        balance -= amount;
        setChanged();
        notifyObservers();

    }

    public String getPlan() {
        switch (plan) {
            default:
                return "None";
            case THREE_MONTHS:
                return "Three Months";
            case ONE_YEAR:
                return "One Year";
            case ONE_MONTH:
                return "One Month";

        }
    }

    public LocalDateTime getEndTime() {
        if (start != null && start.plus(times[plan]).isAfter(LocalDateTime.now())) {
            return start.plus(times[plan]);
        }
        return LocalDateTime.now();
    }

}
