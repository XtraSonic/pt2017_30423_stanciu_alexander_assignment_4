/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.io.Serializable;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Xtra Sonic
 */
public class Person implements Observer,Serializable {

    private final String firstName;
    private final String lastName;
    
    public Person(String firstName,String lastName)
    {
        this.firstName=firstName;
        this.lastName=lastName;
    }

    @Override
    public boolean equals(Object o) {
        if(o==this)
            return true;
        if(o==null)
            return false;
        if(!(o instanceof Person))
            return false;
        Person p=(Person) o;
        
        return p.hashCode()==this.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName,lastName);
    }

    @Override
    public String toString() {
        return  firstName+" "+lastName ;
    }
    
    
    @Override
    public void update(Observable o, Object o1) {
        System.out.println(this.toString()+" has been updated :"+o.toString());
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    
    
}
