/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Xtra Sonic
 */
public class SerialBank implements Serializable{

    protected Map<Person, Set<Account>> bank;
    String fileName;
    protected SerialBank(String fileName) throws IOException, FileNotFoundException, ClassNotFoundException
    {
        this.fileName=fileName;
        File file=new File(fileName);
        if(file.exists()&&file.length()!=0){
            readBank();
        }
        else
        {  
            file.createNewFile();
            bank=new HashMap<>();
        }

    }
    
    protected final void readBank() throws FileNotFoundException, IOException, ClassNotFoundException {
       
        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream ois = new ObjectInputStream(fis);
        bank = (Map<Person, Set<Account>>) ois.readObject();

    }

    protected void writeBank() throws FileNotFoundException, IOException {
        FileOutputStream fis = new FileOutputStream(fileName);
        ObjectOutputStream ois = new ObjectOutputStream(fis);
        ois.writeObject(bank);
    }
}
