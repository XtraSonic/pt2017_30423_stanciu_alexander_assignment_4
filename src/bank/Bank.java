/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Xtra Sonic
 * @inv isWellFormed()==true
 */
public class Bank extends SerialBank implements BankProc{
    
    
    public Bank(String fileName) throws IOException, FileNotFoundException, ClassNotFoundException
    {
        super(fileName);
    }
    

    @Override
    public void addPerson(Person p) {
        assert p!=null : "Null Person can`t be added";
        assert isWellFormed() : "Not well formed";
       
        int initSize=bank.size();
        bank.put(p, new HashSet<>());
       
        assert bank.size()==initSize+1 : "Size didn`t increase by only one when adding a Person";
        assert isWellFormed() : "Not well formed";
    }

    @Override
    public void removePerson(Person p) {
        assert p!=null : "Null Person can`t be removed";
        assert isWellFormed() : "Not well formed";
        
        int initSize=bank.size();
        bank.remove(p);
        
        assert bank.size()==initSize-1 : "Size didn`t decrease by only one when removing a Person";
        assert isWellFormed() : "Not well formed";
    }

    public boolean isWellFormed()
    {
        bank.entrySet().stream().anyMatch(e->{
            return e.getKey()==null;
                });
        return true;
    }
    
    
    @Override
    public void addAccountToPerson(Person p, Account a) {
        assert p!=null : "Null Person can`t be added an account to";
        assert isWellFormed() : "Not well formed";
        assert bank.containsKey(p) : "Person is not in bank";
        assert a!=null : "Can`t add null account";
        
        Set<Account> set=bank.get(p);
        int initialSize= set.size();
        a.addObserver(p);
        set.add(a);
        bank.put(p, set);
        
        
        assert bank.get(p).size()==initialSize+1: "Set size didn`t increase by only 1";
        assert isWellFormed(): "Not well formed";
        
        
    }

    @Override
    public void removeAccountFromPerson(Person p, Account a) {
        
        assert p!=null : "Null Person can`t be removed";
        assert isWellFormed() : "Not well formed";
        assert bank.containsKey(p) : "Person is not in bank";
        assert a!=null : "Can`t remove null account";
        assert bank.get(p).contains(a) : "Person doesn`t contain the account";
        
        Set<Account> set=bank.get(p);
        int initialSize= set.size();
        a.deleteObserver(p);
        set.remove(a);
        bank.put(p, set);
        
        assert bank.get(p).size()==initialSize-1: "Set size didn`t increase by only 1";
        assert isWellFormed(): "Not well formed";
        
        
    }

    @Override
    public Account[] readAccountData(Person p) {
        assert p!=null : "Null Person can`t be removed";
        assert isWellFormed() : "Not well formed";
        assert bank.containsKey(p) : "Person is not in bank";
        
        Object[] temp=bank.get(p).toArray();
        Account[] result = new Account[temp.length];
        for(int i=0;i<temp.length;i++)
        {
            result[i]=(Account)temp[i];
        }
        
        assert isWellFormed() : "Not well formed";
        
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb=new StringBuilder();
        bank.forEach((k, v)->{sb.append(k.toString()).append(" has the accounts:").append(v.toString()).append("\n");});
        return sb.toString();
    }

    @Override
    public void writeBank() throws FileNotFoundException, IOException {
        super.writeBank(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Person[] getPersons()
    {
        Object[] temp=bank.keySet().toArray();
        Person[] result = new Person[temp.length];
        for(int i=0;i<temp.length;i++)
        {
            result[i]=(Person)temp[i];
        }
        
        return result;
    }

    public boolean isEmpty() {
        return bank.isEmpty();
    }
}
