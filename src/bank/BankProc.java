/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bank;

/**
 *
 * @author Xtra Sonic
 */
public interface BankProc {
    
    /**
     * adds a person to the bank
     * @param p Person to be added to the bank
     * @pre p!=null
     * @pre isWellFormed()==true
     * @post isWellFormed()==true
     * @post finalSize == initialSize+1 
     */
    public void addPerson(Person p);
    
    /**
     * removes a person to the bank
     * @param p Person to be removed from the bank
     * @pre p!=null
     * @pre isWellFormed()==true
     * @post isWellFormed()==true
     * @post finalSize == initialSize-1 
     */
    public void removePerson(Person p);
    
    
    /**
     * add an account to a person
     * @param p Person to whom to link the account to
     * @param a The Account to be linked
     * @pre p!=null
     * @pre p is in bank
     * @pre a!=null
     * @pre isWellFormed()==true
     * @post finalSetSize = iniSetSize+1
     * @post isWellFormed()==true
     */
    public void addAccountToPerson(Person p,Account a);
    
    /**
     * remove an account from a person
     * @param p Person from whom to unlink the account from
     * @param a The Account to be unlinked
     * @pre p!=null
     * @pre p is in bank
     * @pre a!=null
     * @pre isWellFormed()==true
     * @pre a is in set
     * @post finalSetSize = iniSetSize-1
     * @post isWellFormed()==true
     */
    public void removeAccountFromPerson(Person p,Account a);
    
    /**
     * 
     * @param p person for whom we'll read the accounts from
     * @return the accounts as an object array
     * @pre p!=null
     * @pre p is in bank
     * @pre isWellFormed()==true
     * @post isWellFormed()==true
     */
    public Object[] readAccountData(Person p);
   
}
